package com.hendisantika.springbootfeign;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-feign
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/01/18
 * Time: 07.46
 * To change this template use File | Settings | File Templates.
 */
@FeignClient(value = "customers", path = "/customers_mto")
public interface CustomerClient {

    @RequestMapping(method = RequestMethod.GET, value = "/")
    Resources<Customer> getCustomers();

    @RequestMapping(method = RequestMethod.GET, value = "/{id}")
    Resource<Customer> getCustomer(@PathVariable("id") long id);

}
