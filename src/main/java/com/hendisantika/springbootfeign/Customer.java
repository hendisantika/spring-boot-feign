package com.hendisantika.springbootfeign;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-feign
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/01/18
 * Time: 07.45
 * To change this template use File | Settings | File Templates.
 */
public class Customer {

    private long id;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}