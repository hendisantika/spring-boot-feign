package com.hendisantika.springbootfeign;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-feign
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/01/18
 * Time: 07.44
 * To change this template use File | Settings | File Templates.
 */
public class Address {
    private long id;

    private String street;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
