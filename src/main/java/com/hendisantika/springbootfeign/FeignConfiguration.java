package com.hendisantika.springbootfeign;

import feign.Logger;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-feign
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 23/01/18
 * Time: 07.47
 * To change this template use File | Settings | File Templates.
 */
@Configuration
public class FeignConfiguration {

    @Bean
    public Logger.Level logLevel() {
        return Logger.Level.FULL;
    }

}
